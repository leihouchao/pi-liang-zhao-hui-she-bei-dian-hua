console.log("******* 开始查找 ****************");

// 1,excel文件读写
var xlsx = require('node-xlsx');
var fs = require('fs');

// 2,注册leancloud
var APP_ID = '5wk8ccseci7lnss55xfxdgj9xn77hxg3rppsu16o83fydjjn'
var APP_KEY = 'yovqy5zy16og43zwew8i6qmtkp2y6r9b18zerha0fqi5dqsw'
var AV = require('leancloud-storage');
AV.init({
    appId: APP_ID,
    appKey: APP_KEY
});
/*
  功能: 文件读 
    输入: 文件名filename
    返回: imei数组
*/
var getImei = function(filename){
    var obj = xlsx.parse(filename);
    var excelObj=obj[0].data;

    // 读取imei
    var imeis = [];
    for(var i = 1;i < excelObj.length; i++){
        imeis[i-1] = excelObj[i][1];
    }

    // 打印一下
    console.log("共有: ",imeis.length,' 个设备');

    // 返回
    return imeis;
}
/*
  功能: 文件写,将电话写入到文件filename中
    输入: 文件名filename,电话列表tels
*/
var writeTel = function(filename,tels){
    // 1,读取原来的表结构
    var obj = xlsx.parse(filename);
    var excelObj=obj[0].data;

    // 2,将电话赋值进去
    for(var i = 1;i < excelObj.length; i++){
        excelObj[i][3] = tels[i-1];
    }

    // 3,构建新表
    var buff = xlsx.build([
        {
            name:"查询设备的电话",
            data:excelObj
        }
    ])

    // 4,写入新表
    fs.writeFileSync(filename,buff,{'flag':'w'})
}
/*
  功能: 查找输入设备imei的电话,有多个电话时只返回一个电话
    输入: 设备号
    返回: 设备号对应的手机号
*/
async function searchTel(imei){
    // 1,设置设备的查找条件
    var Bindings = new AV.Query('Bindings');
    var _User = new AV.Query('_User');
    Bindings.equalTo('IMEI',imei);

    // 2,开始查找设备
    try{
        var res = await Bindings.find();
        // 3,找寻objId
        var objId = res[0].get('user').get('objectId');
        // 4,查找设备的user
        var user = await _User.get(objId);
        // 5,查找user的手机号
        let tel = user.get('username');
        console.log("***** 设备: ",imei,' ***** 手机号: ',tel,' ****************');
        return tel;
    }catch(err){
        console.log("设备 ",imei,' 查找失败');
        return null;
    }
}

/*
    主函数: main
*/
var main = function(filename){
    // 1,读取设备号imei
    var imeis = getImei(filename);
    // 2,查询电话
    var tels = [];
    (async() => {
        // 2,读取电话
        for(var i = 0;i < imeis.length ;i ++){
            var tel = await searchTel(imeis[i]);
            tels[i] = tel;
        }
        console.log("电话号码是: ",tels);  
        writeTel(filename,tels); 
    })();
    
    // var timLen = imeis.length * 500 + 100;
    // setTimeout(function(){
    //     console.log("电话有: ",this.TELS);
    // },timLen); 

}

main('./test.xlsx');